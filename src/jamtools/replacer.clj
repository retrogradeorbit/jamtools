(ns jamtools.replacer
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.java.io :refer [file]]
            [clojure.string :refer [split]])
  (:import [ij IJ ImagePlus]
           [java.awt Color Font Graphics2D]
           [java.awt.image BufferedImage]
           [javax.imageio ImageIO])
  (:gen-class))

(set! *warn-on-reflection* false)

(def help-text
  "replace-colour
=================
Replace one colour in the image with another and save as a new image.")

(def cli-options
  [["-i" "--image FILE" "load the image from this file"]
   ["-s" "--search COLOUR" "replace this colour"]
   ["-r" "--replace COLOUR" "colour to replace it with"]
   ["-o" "--output FILE" "save the resultant image into this file"]
   ["-d" "--display" "Show the new image on the screen"]
   ["-h" "--help" "Help information"]])

(def colours
  {:red [255 0 0]
   :green [0 255 0]
   :blue [0 0 255]
   :magenta [255 0 255]
   :cyan [0 255 255]
   :yellow [255 255 0]
   :white [255 255 255]
   :black [0 0 0]
   :gray [127 127 127]
   })

(defn- parse-hex-colour [colour]
  (for [pair (partition 2 colour)] (Integer/parseInt (apply str pair) 16)))

(defn- parse-colour [colour]
  (cond
   (re-find #"^[a-fA-F0-9]{6}$" colour)
   (parse-hex-colour colour)

   :else    
   ((keyword colour) colours)))

(defn -main
  "Load in an image and replace all pixels of one colour with another colour"
  [& args]
  (let [parsed (parse-opts args cli-options)
        opts (:options parsed)]
    (cond
     ;; help
     (:help opts)
     (do
       (println help-text)
       (println (:summary parsed)))

     ;; search and replace
     (every? opts [:image :search :replace])
     (let [fn (:image opts)
           out (:output opts)
           i (IJ/openImage fn)
           b (.getBufferedImage i)
           w (.getWidth i)
           h (.getHeight i)
           s (parse-colour (:search opts))
           r (parse-colour (:replace opts))

           ; replacement pixel value integer
           rp (reduce bit-or (map #(bit-shift-left %1 %2) r [16 8 0 24]))

           ; list of pixels that need changing
           pl (for [x (range w) y (range h)
                    :let [px (.getRGB b x y)
                          [a r g b] (for [sh [24 16 8 0]]
                                        ; extract a r g b
                                      (-> px (bit-shift-right sh) (bit-and 0xff)))]
                    :when (= [r g b] s)] [x y])]
       ; replace the colour
       (loop [p pl]
         (if (not (empty? p))
           (let [[x y] (first p)]
             (.setRGB b x y rp)
             (recur (rest p)))
           (do
             ; --display
             (if (:display opts)
               (.show i))
             ; --output
             (if out (ImageIO/write b (last (split out #"\.")) (file out)))))))
     
     ;; default
     :else
     (do
       (println help-text)
       (println (:summary parsed))))))
