(ns jamtools.core
  (:require [clojure.tools.cli :refer [parse-opts]])
  (:import [ij IJ ImagePlus]
           [java.awt Color Font Graphics2D]
           [java.awt.image BufferedImage])
  (:gen-class))

(set! *warn-on-reflection* false)

(def help-text
  "tile-numbers
=============
Take an image and number all the regular square tiles, either row by row, or column
by column.")

(def cli-options
  [["-i" "--image FILE" "load the image from this file"]
   ["-s" "--size PIXELS" "The size of each tile in pixels."
    :default 24
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 512) "Must be an integer between 0 and 512"]]
   ["-o" "--offset NUM" "The number to start counting at"
    :default 0
    :parse-fn #(Integer/parseInt %)]
   ["-S" "--scale SCALE" "The factor to expand the output image to make it easy to read"
    :default 1
    :parse-fn #(Integer/parseInt %)
    :validate [#(> % 0) "Must be a positive integer"]]
   [nil "--column-first" "Number by column first"]
   [nil "--row-first" "Number by row first"]
   [nil "--white" "Write text and lines in a white colour"]
   [nil "--grey" "middle grey colour"]
   [nil "--red" "red colour"]
   [nil "--green" "green colour"]
   [nil "--blue" "blue colour"]
   ["-h" "--help" "Help information"]])

(defn- draw-grid [g2d w h s]
  "grid drawing monad." 
  (doseq [x (range 0 w s)]
    (.drawLine g2d x 0 x h))
  (doseq [y (range 0 h s)]
    (.drawLine g2d 0 y w y)))

(defn- annotate-boxes [g2d font tx ty s func]
  "annotate the boxes using font with text from calling func with x,y coords"
  (.setFont g2d font)
  (let [fm (.getFontMetrics g2d)
        fh (.getHeight fm)]
    (doseq [x (range 0 tx) y (range 0 ty)]
      (.drawString
       g2d
       (str (func x y))
       (+ 1 (* x s))
       (+ -2 fh (* y s))))))

(defn -main
  "Load in an image and draw a numbered grid over it"
  [& args]
  (let [parsed (parse-opts args cli-options)
        opts (:options parsed)]

    (cond
     ;; help
     (:help opts) (do
                    (println help-text)
                    (println (:summary parsed)))

     ;; both numbering schemes at once
     (and (:column-first opts) (:row-first opts))
      (do
       (println "ERROR: cannot count both column and row first")
       (println (:summary parsed))
       (System/exit 1))

     ;; multiple colours at once
     (> (count (filter #{:red :green :blue :grey :white} (keys opts))) 1)
      (do
       (println "ERROR: can only specify one colour at a time")
       (println (:summary parsed))
       (System/exit 1))
     
     (:image opts)
      (let [file (:image opts)
            offset (:offset opts)
            scale (:scale opts)
            i (IJ/openImage file)
            w (.getWidth i)
            h (.getHeight i)
            depth (.getBitDepth i)
            obi (.getBufferedImage i)
            
            ;; tilesize and number of x tiles and number
            ;; of y tiles
            s (:size opts)
            tx (quot w s)
            ty (quot h s)
            
            ;; scaled equivalents
            sw (* w scale)
            sh (* h scale)
            ss (* s scale)
            stx (* tx scale)
            sty (* ty scale)
            
            bi (BufferedImage. sw sh BufferedImage/TYPE_INT_ARGB)
            g2d (.createGraphics bi)
            sg2d (.createGraphics bi)           ; for scaling
            font (Font. "monospaced" Font/PLAIN 8)
            col (cond (:white opts) Color/white
                      (:grey opts) Color/gray
                      (:red opts) Color/red
                      (:green opts) Color/green
                      (:blue opts) Color/blue
                      :else Color/darkGray)]
        
        (doto sg2d
          ;; scale the old image 
          (.scale scale scale)
          (.drawImage obi 0 0 nil)
          (.dispose))
        
        (doto g2d
          ;; set the colour
          (.setPaint col)
          
          ;; draw the grid
          (draw-grid sw sh ss)
          
          ;; count the boxes
          (annotate-boxes font stx sty ss
                          (cond
                           (:column-first opts) (fn [x y] (+ offset x (* tx y)))
                           (:row-first opts) (fn [x y] (+ offset y (* ty x)))
                           :else (fn [x y] (str (+ offset x) "," (+ offset y)))))
          
          (.dispose)) 
        (.show (ImagePlus. "output" bi)))

      :else (do
              (println help-text)
              (println (:summary parsed))))))
