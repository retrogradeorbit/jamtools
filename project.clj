(defproject jamtools "0.1.0-SNAPSHOT"
  :description "Gamejam tools"
  :url "https://bitbucket.org/retrogradeorbit/jamtools"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  ;; :repositories {"imagej.releases" "http://maven.imagej.net/content/repositories/releases"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.cli "0.3.1"]
                 ;; [net.imagej/ij "1.45b"]
                 [imagej/ij "1.43"]
                 ]
  :main ^:skip-aot jamtools.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  ;; :aot [jamtools.core]
  :aliases {"tile" ["run" "-m" "jamtools.core"]
            "replacer" ["run" "-m" "jamtools.replacer"]})
