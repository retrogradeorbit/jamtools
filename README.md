# jamtools

Some gamejam tools.

## Installation

Download from https://bitbucket.org/retrogradeorbit/jamtools/

## Usage

This is at the moment just one utility. It will show you any image with a grid layed out over it
and the grid squares numbered. You can use this to quickly get co-ordinates on texture maps and
sprite sheets being used in 2D game design.

    $ java -jar jamtools-0.1.0-standalone.jar [args]
    
or
    
    $ lein tile -- -h

or

    $ lein replacer -- -h

## Options

tile
----

   * -i imagefile
   * -s tile size
   * -o count offset

Colour of the grid and numbers can be controlled with

   * --green
   * --blue
   * --red
   * --grey
   * --white

Choose the numbering by leaving on default, or using

   * --column-first
   * --row-first

## Examples

    $ lein tile -- -h

    tile-numbers
    =============
    Take an image and number all the regular square tiles, either row by row, or column
    by column.
       -i, --image FILE        load the image from this file
       -s, --size PIXELS   24  The size of each tile in pixels.
       -o, --offset NUM    0   The number to start counting at
       -S, --scale SCALE   1   The factor to expand the output image to make it easy to read
           --column-first      Number by column first
           --row-first         Number by row first
           --white             Write text and lines in a white colour
           --grey              middle grey colour
           --red               red colour
           --green             green colour
           --blue              blue colour
       -h, --help              Help information

A large image with large tiles numbered from 1:

    $ lein tile -i input-image.png -s 32 -o 1 --column-first 

A smaller image with small 8 pixel tiles anotated in green:

    $ lein tile -i input-image.png -s 8 --scale 3 --green

Replace one colour with another

    $ lein replacer -s red -r blue -i infile.png -o outfile.png

### Bugs

Plenty.

## License

Copyright © 2014 Crispin Wellington

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
